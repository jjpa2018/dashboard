<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CuestionarioExportC0 implements FromArray,WithHeadings
{
    protected $cuestionario;


    public function __construct(array $cuestionario)
    {
        $this->cuestionario = $cuestionario;
    }

    public function headings(): array
    {
        return [
            '¿Quién participará del estudio?',
            'Relación con el menor de edad',
            'Cuál es tu nombre completo',
            '¿Cuenta con cédula panameña?',
            'Número de cédula panameña o de pasaporte',
            'Cuál es tu fecha de nacimiento (dd/mm/aaaa)',
            '¿Ya recibió alguna dosis de la vacuna contra la COVID-19?',
            '¿Cuál fue la fecha de aplicación de la primera dosis de la vacuna?',
            '¿Cuántos días han pasado desde el momento de la vacunación de la primera dosis y el llenado de este cuestionario?',
            'Cuando planifica vacunarse?',
            'Proceso de Consentimiento/Asentimiento',
            '¿Cuenta con cédula panameña?',
            '¿Cuál es el número de cédula?',
            '¿Cuál es el número del documento de identificación o pasaporte?',
            '¿Cuál es el nombre completo?',
            '¿Cuál es la fecha de nacimiento?',
            '¿Ha recibido alguna otra dosis de la vacuna contra la COVID-19?',
            'correo electronico',
        ];
    }
    
    public function array(): array
    {
        return $this->cuestionario;
    }
}
