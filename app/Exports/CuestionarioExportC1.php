<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CuestionarioExportC1 implements FromArray,WithHeadings
{
    protected $cuestionario;


    public function __construct(array $cuestionario)
    {
        $this->cuestionario = $cuestionario;
    }

    public function headings(): array
    {
        return [
            '¿Tienes un número de teléfono fijo?',
            '¿Cuál es tu número de  teléfono fijo ?',
            'nombre completo de esa persona',
            '¿Cuál es el número de teléfono celular de esta persona?',
            '¿Podrías darnos un número de teléfono fijo de esta persona?',
            '¿Cuál es el número de  teléfono fijo  de esta persona?',
            'Esta persona ¿Tiene correo electrónico?',
            '¿Cuál es el correo electrónico de esta persona?',
            '¿En que  provincia  vives?',
            '¿En cuál  distrito ?',
            '¿En cual  corregimiento ?',
            '¿En cuál  lugar poblado ,  barriada  o  sector ?',
            '¿Cuál es la dirección donde vives actualmente?',
            '¿Cuál es tu  sexo ?',
            '¿Cuál es tu estado actual?',
            '¿Estás amamantando?',
            '¿Cuánto pesas?',
            '¿Cuánto mides?',
            '¿Padeces alguna  discapacidad ?',
            'Esta discapacidad ¿te impide movilizarte?',
            '¿Padeces alguna  enfermedad crónica ?',
            'Enfermedad Cardiovascular',
            'Cáncer',
            'Diabetes',
            'Enfermedad Pulmonar Obstructiva Crónica',
            'Hipertensión',
            'Obesidad',
            'Enfermedad renal crónica',
            'VIH',
            'Asma',
            'Enfermedad del Hígado',
            'Alergia Crónica',
            'Otra enfermedad',
            '¿Cuál es tu nivel máximo de estudios que realizaste?',
            '¿Fueron completos o incompletos los estudios?',
            '¿Eres asegurado por la Caja de Seguro Social?',
            '¿Cuál es tu estatus en el seguro social?',
            '¿Cuál es el ingreso mensual por jubilación o pensión en dólares?',
            '¿Cuál es su ocupación actual?',
            '¿Cuánto es su ingreso mensual completo en dólares?',
            '¿Es un trabajador formal o informal?',
            '¿Cuál es tu estatus laboral actualmente?',
            'Por favor, indica que contrato tienes.',
            'El participante del estudio ¿Presentó la enfermedad COVID-19 previa a la vacunación?',
            'fecha de tu diagnóstico',
            'El participante ¿Ha presentado reacción a vacunaciones previas?',
            '¿A qué vacuna tuvo reacción?',
            'Por favor, describe la reacción que tuviste.',
            'El participante ¿ha tomado  medicamentos  en los tres días medios a la vacunación?',
            'Nombre del mediamento 1',
            'Uso medicamento 1',
            'Nombre del mediament 2',
            'Uso medicamento 2',
            'Nombre del mediament 3',
            'Uso medicamento 3',
            'Nombre del mediament 4',
            'Uso medicamento 4',
            '¿Eres alérgico/a a medicamentos?',
            'Por favor, especifica a que eres alérgico/a',
            '¿Eres alérgico/a a alimentos?',
            'Por favor, especifica a que eres alérgico/a',
            '¿Eres alérgico/a a otras sustancias?',
            'Por favor, especifica a que eres alérgico/a',
        ];
    }
    
    public function array(): array
    {
        return $this->cuestionario;
    }
}
