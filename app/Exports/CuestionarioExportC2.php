<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CuestionarioExportC2 implements FromArray,WithHeadings
{
    protected $cuestionario;


    public function __construct(array $cuestionario)
    {
        $this->cuestionario = $cuestionario;
    }

    public function headings(): array
    {
        return [
            '¿Tuvo  fiebre  3 días antes de la vacunación ?',
            '¿Se logró medir la  temperatura ?',
            '¿Cuál fue el valor de la  temperatura ?',
            '¿Sentió  naúseas  o  vómitos  ?',
            'Que tan fuerte fueron las  nauseas  o  vómitos ?',
            '¿Experimentó  malestar general  ?',
            'Que tan fuerte fue el  malestar general ?',
            '¿Tuviste  escalofríos  ?',
            'Los escalofríos ¿impidieron tus actividades diarias?',
            '¿Tuviste  dolor de cabeza ?',
            'El dolor de cabeza ¿impidió tus actividades diarias?',
            '¿Tuviste  dolor en las articulaciones  ?',
            'El dolor articular ¿impedía tus actividades diarias?',
            '¿Tuviste  dolores musculares   ?',
            'El dolor muscular ¿impidió tus ocupaciones?',
            '¿Sentiste  cansancio  o  fatiga  ?',
            'El cansancio ¿impidió tus actividades?',
            '¿Tuviste algún  otro síntoma ?',
            'Por favor, especifica cuál fue el  síntoma ',
            'Este último síntoma ¿impidió tus actividades?',
        ];
    }
    
    public function array(): array
    {
        return $this->cuestionario;
    }
}
