<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CuestionarioExportC3 implements FromArray,WithHeadings
{
    protected $cuestionario;


    public function __construct(array $cuestionario)
    {
        $this->cuestionario = $cuestionario;
    }

    public function headings(): array
    {
        return [
            '¿En cuál centro de vacunación asististe?',
            '¿Cuál fue la  fecha de vacunación ?',
            '¿Cuál fue el  fabricante de la vacuna  que te aplicaron?',
            'Por favor, detalla cuál fue.',
            '¿Cuál es el  número de lote  de la vacuna que te aplicaron?',
            '¿Te has vacunado contra  cualquier enfermedad  que no sea COVID-19 en el último año?',
            '¿Contra qué enfermedad te vacunaron?',
            '¿Te aplicaste  otra vacuna  el mismo día que te administraron la vacuna de la COVID-19?',
            '¿Contra qué enfermedad te vacunaron?',
            '¿Qué  alimentos  consumiste previo a la vacunación?',
            '¿Qué  bebidas  consumiste previo a la vacunación?',
            '¿Tomaste algún medicamento antes de la vacunación?',
            '¿Cuántos medicamentos tomaste o aplicaste?',
            'por favor, escribe el nombre de cada  medicamentos  que tomaste antes de vacunarte',
        ];
    }
    
    public function array(): array
    {
        return $this->cuestionario;
    }
}
