<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CuestionarioExportC4A2 implements FromArray,WithHeadings
{
    protected $cuestionario;


    public function __construct(array $cuestionario)
    {
        $this->cuestionario = $cuestionario;
    }

    public function headings(): array
    {
        return [
            'En el cuestionario anterior ¿reportaste algún  síntoma ?',
            '¿Experimentaste algún síntoma en esta última semana?',
            'En comparación con los síntomas reportados anteriormente ¿Cómo te has sentido?',
            '¿Cuántos disminuyeron?',
            'Por favor, indícanos cuáles',
            '¿Cuántos aumentaron?',
            'Por favor, indícanos cuáles',
            '¿Cuántos desaparecieron?',
            'Por favor, indícanos cuáles',
            '¿Se te presentó otro síntoma diferente a los que sentiste en la semana anterior?',
            '¿Cuál fue el síntoma que presentaste?',
            '¿Buscaste atención médica para cualquiera de los síntomas? Sea para los síntomas anteriores o nuevos',
            '¿A qué instalación de salud acudiste?',
            '¿Cuál es el nombre de la instalación médica?',
            '¿Te hicieron algún examen de laboratorio o de imágenes?',
            'Prueba de Orina',
            'Prueba de Sangre',
            'Ultrasonido',
            'Radiografia',
            'otro examen',
            '¿Qué otro exámen te hicieron?',
            '¿Te indicaron un medicamento?',
            '¿Cuál medicamento te indicaron?',
            '¿Utilizaste algún medicamento para aliviar el (o los) síntoma?',
            'Por favor, especifica los medicamentos utilizados',
            'El medicamento, ¿fue prescrito por un médico o lo tomó/aplicó por cuenta propia?',
            '¿Durante la última semana requeriste hospitalización?',
            '¿Por cuál motivo?',
            '¿Se han resuelto tus síntomas?',
            '¿Deseas realizar alguna consulta con un profesional de nuestro equipo de investigación?',
            '¿Cómo quieres que te contactemos?',
            '¿Ha tomado  medicamentos  en los tres días medios a la vacunación?',
            'medicamento1',
            'uso del medicamento 1',
            'quien receto 1',
            'medicamento2',
            'uso del medicamento 2',
            'quien receto 2',
            'medicamento3',
            'uso del medicamento 3',
            'quien receto 3',
            'medicamento4',
            'uso del medicamento 4',
            'quien receto 4',
            'Desde la ultima vez que hablamos ¿tuvo una prueba positiva de COVID-19?',
            '¿Recuerda la fecha aproximada de la prueba?',
            'Anote la fecha de la prueba',
            '¿Recibió la segunda dosis de vacuna contra la COVID-19? ',
        ];
    }
    
    public function array(): array
    {
        return $this->cuestionario;
    }
}
