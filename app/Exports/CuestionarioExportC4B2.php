<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CuestionarioExportC4B2 implements FromArray,WithHeadings
{
    protected $cuestionario;


    public function __construct(array $cuestionario)
    {
        $this->cuestionario = $cuestionario;
    }

    public function headings(): array
    {
        return [
            '¿En el cuestionario anterior, reportó algún síntoma?',
            '¿Ha experimentado algún síntoma esta última semana?',
            '¿Cómo se ha sentido esta semana, de acuerdo con los síntomas de la semana anterior?',
            '¿Se presentó otro (s) síntoma(s) diferente (s) a los que sintió en la semana anterior?',
            '¿Cuál fue el (o los) síntoma (s) que presentó?',
            '¿Buscó atención médica para cualquiera de los síntomas, ya sea para los síntomas anteriores o los nuevos',
            '¿A donde acudió?',
            '¿ Cuál es el nombre de esa instalación de salud a la que acudió?',
            '¿Le indicaron algún examen de laboratorio o de imágenes?',
            'Prueba de Orina',
            'Prueba de Sangre',
            'Ultrasonido',
            'Radiografia',
            'otro examen',
            '¿Cuál es el nombre de ese examen?',
            '¿Le indicaron algún medicamento?',
            '¿Qué medicamento le indicaron?',
            '¿Utilizó algún medicamento para aliviar el (o los) síntoma (s) que experimentó (s)?',
            '¿Cuál es el nombre del medicamento?',
            'El medicamento',
            '¿Durante el último mes requirió hospitalización?',
            '¿Por cuál motivo?',
            '¿Cómo evolucionó la condición que experimentó?',
            '¿Desea realizar alguna consulta con un profesional de nuestro equipo de investigación?',
            '¿Por qué medio deseas que lo contactemos?',
            '¿Ha empezado a tomar algún medicamento desde la última vez que nos contactamos?',
            'medicamento1',
            'uso del medicamento 1',
            'quien receto 1',
            'medicamento2',
            'uso del medicamento 2',
            'quien receto 2',
            'medicamento3',
            'uso del medicamento 3',
            'quien receto 3',
            'medicamento4',
            'uso del medicamento 4',
            'quien receto 4',
            'Desde la última vez que hablamos ¿tuvo alguna prueba positiva de COVID-19?',
            '¿Recuerda la fecha aproximada de la prueba?',
            'Anote la fecha de la prueba',
            '¿ Recibió la tercera dosis de vacuna contra la COVID-19?',
        ];
    }
    
    public function array(): array
    {
        return $this->cuestionario;
    }
}
