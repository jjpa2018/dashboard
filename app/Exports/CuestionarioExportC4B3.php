<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CuestionarioExportC4B3 implements FromArray,WithHeadings
{
    protected $cuestionario;


    public function __construct(array $cuestionario)
    {
        $this->cuestionario = $cuestionario;
    }

    public function headings(): array
    {
        return [
            '¿Experimentó algún síntoma  por la dosis aplicada?',
            '¿Cuál fue el síntoma que presentaste?',
            '¿Buscaste atención médica para cualquiera de los síntomas? ',
            '¿A qué instalación de salud acudiste? ',
            '¿Cuál es el nombre de la instalación médica?',
            '¿Te hicieron algún examen de laboratorio o de imágenes?',
            'Prueba de Orina',
            'Prueba de Sangre',
            'Ultrasonido',
            'Radiografia',
            'otro examen',
            '¿Cuál es el nombre de ese examen?',
            ' ¿Le indicaron algún medicamento?',
            '¿Qué medicamento le indicaron?',
            '¿Utilizaste algún medicamento para aliviar el (o los) síntoma?',
            'Por favor, especifica los medicamentos utilizados:',
            'El medicamento, ¿fue prescrito por un médico o lo tomó/aplicó por cuenta propia?',
            'Requeriste hospitalización como resultado de esa reacción?',
            '¿Por cuál motivo?',
            '¿Cómo evolucionó la condición que experimentaste?',
            '¿Deseas realizar alguna consulta con un profesional de nuestro equipo de investigación?',
            '¿Cómo quieres que te contactemos?',
            '¿Has tomado  medicamentos  desde la ultima vez que hablamos? ',
            'medicamento1',
            'uso del medicamento 1',
            'quien receto 1',
            'medicamento2',
            'uso del medicamento 2',
            'quien receto 2',
            'medicamento3',
            'uso del medicamento 3',
            'quien receto 3',
            'medicamento4',
            'uso del medicamento 4',
            'quien receto 4',
            'Desde la última vez que hablamos ¿tuviste una prueba positiva?',
            '¿Recuerdas la fecha aproximada?',
            'Por favor, indica la fecha de la prueba.',
            '¿Recibiste la siguiente dosis contra el Covid-19?',
        ];
    }
    
    public function array(): array
    {
        return $this->cuestionario;
    }
}
