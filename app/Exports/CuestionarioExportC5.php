<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CuestionarioExportC5 implements FromArray,WithHeadings
{
    protected $cuestionario;


    public function __construct(array $cuestionario)
    {
        $this->cuestionario = $cuestionario;
    }

    public function headings(): array
    {
        return [
            '¿En el cuestionario anterior, reportó algún síntoma?',
            '¿Experimentó  algún síntoma el último mes?',
            'Nos interesa saber ¿Cómo se ha sentido este mes, de acuerdo con los síntomas del mes anterior?',
            '¿Cuántos disminuyeron?',
            'Por favor, indícanos cuáles',
            '¿Cuántos aumentaron?',
            'Por favor, indícanos cuáles',
            '¿Cuántos desaparecieron?',
            'Por favor, indícanos cuáles',
            '¿Presentó otro (s) síntoma(s) diferente (s) al (los) que sintió  el mes anterior?',
            ' ¿Cuál (es) fue (fueron) el (los) síntoma (s) que presentó(aron)? ',
            '¿Buscó atención médica para cualquiera de los síntomas (los anteriores, los nuevos o ambos)?',
            ' ¿A donde acudió?',
            '¿ Cuál es el nombre de esa instalación de salud a la que acudió?',
            '¿Le indicaron algún examen de laboratorio o de imágen? ',
            'Prueba de Orina',
            'Prueba de Sangre',
            'Ultrasonido',
            'Radiografia',
            'otro examen',
            '¿Qué examen de laboratorio o imagen le realizaron?',
            '¿Cuál es el nombre de ese examen?',
            '¿Le indicaron algún medicamento?',
            '¿Qué medicamento le indicaron?',
            '¿Utilizó algún medicamento para aliviar el (o los) síntoma (s) que experimentó (s)?',
            ' ¿Cuál es el nombre del medicamento?',
            ' El medicamento:',
            ' ¿Durante el último mes requirió  hospitalización? ',
            '¿Por cuál motivo?',
            '¿Cómo evolucionó la condición que experimentó?',
            '¿Desea realizar alguna consulta con un profesional de nuestro equipo de investigación?',
            '¿Por qué medio desea ser contactado?',
            '¿Ha empezado a tomar algún medicamento  desde la última vez que nos contactamos?',
            'medicamento1',
            'uso del medicamento 1',
            'quien receto 1',
            'medicamento2',
            'uso del medicamento 2',
            'quien receto 2',
            'medicamento3',
            'uso del medicamento 3',
            'quien receto 3',
            'medicamento4',
            'uso del medicamento 4',
            'quien receto 4',
            'Desde la última vez que hablamos ¿tuvo alguna prueba positiva de COVID-19?',
            '¿Recuerda la fecha aproximada de la prueba?',
            'Anote la fecha de la prueba',
            '¿ Ha recibido otra  dosis de alguna vacuna contra la COVID-19?',
        ];
    }
    
    public function array(): array
    {
        return $this->cuestionario;
    }
}
