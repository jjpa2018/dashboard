<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CuestionarioExportCR1B implements FromArray,WithHeadings
{
    protected $cuestionario;


    public function __construct(array $cuestionario)
    {
        $this->cuestionario = $cuestionario;
    }

    public function headings(): array
    {
        return [
            '¿En el cuestionario anterior, reportó algún síntoma? ',
            '¿Experimentó algún síntoma  las últimas 24 horas? ',
            'Nos interesa conocer¿Cómo se siente hoy de acuerdo con los síntomas reportados previamente ?',
            '¿Cuántos disminuyeron?',
            'Por favor, indícanos cuáles',
            '¿Cuántos aumentaron?',
            'Por favor, indícanos cuáles',
            '¿Cuántos desaparecieron?',
            'Por favor, indícanos cuáles',
            '¿Se  presentó (aron)  otro (s) síntoma(s) diferente (s) a los que sintió  los días anteriores?',
            '¿Cuál (es)  fue (ron) el (los) síntoma (s) que presentó? ',
            ' ¿Buscó atención médica para cualquiera de los síntomas, ya sea para los síntomas anteriores o los nuevos?',
            ' ¿A donde acudió?',
            ' ¿ Cuál es el nombre de esa instalación de salud a la que acudió?',
            'Producto de esta reacción requirió hospitalización',
            '¿Cual hospital ?',
            '¿Le indicaron algún examen de laboratorio o de imágenes?',
            'Prueba de Orina',
            'Prueba de Sangre',
            'Ultrasonido',
            'Radiografia',
            'otro examen',
            '¿Cuál es el nombre de ese examen?',
            '¿Le indicaron algún medicamento?',
            '¿Qué medicamento le indicaron?',
            '¿Cómo evolucionó la condición que experimentó?',
            '¿Utilizó algún medicamento para aliviar el (o los) síntoma (s) que experimentó (s)?',
            '¿Cuál es el nombre del medicamento?',
            'El medicamento:',
            '¿Durante las últimas 24 horas requirió  hospitalización por otra causa?',
            '¿Por cuál motivo?',
            'Desde la última vez que hablamos ¿tuvo alguna prueba positiva de COVID-19?',
            '¿Recuerda la fecha aproximada de la prueba?',
            'Anote la fecha de la prueba',
            ' ¿Desea realizar alguna consulta con un profesional de nuestro equipo de investigación?',
            ' ¿Por qué medio deseas que lo contactemos?',
        ];
    }
    
    public function array(): array
    {
        return $this->cuestionario;
    }
}
