<?php

namespace App\Extensions;

use GuzzleHttp\Client;

class Curls
{

    /**
     *
     *
     * @param
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getDataGlobal()
    {

        try {

            $client = new Client([
                'base_uri' => env('API_SERVER')
            ]);

            $res = $client->request('GET', '/api/cuadromando/data', [
                'auth' => ['mando_nora', 'mando_4p1n0r4'],
                'headers' => ['Content-Type' => 'application/json']
            ]);

            if ($res && $res->getStatusCode() == '200') {
                $res = ($res->getBody()->getContents());
                return $res;
            } else {
                return "nada";
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = (json_decode($e->getResponse()->getBody()->getContents()));
            return $res;
        }
    }

    /**
     *
     *
     * @param
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getEstGenero()
    {

        try {

            $client = new Client([
                'base_uri' => env('API_SERVER')
            ]);

            $res = $client->request('GET', '/api/cuadromando/genero', [
                'auth' => ['mando_nora', 'mando_4p1n0r4'],
                'headers' => ['Content-Type' => 'application/json']
            ]);

            if ($res && $res->getStatusCode() == '200') {
                $res = ($res->getBody()->getContents());
                return $res;
            } else {
                return "nada";
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = (json_decode($e->getResponse()->getBody()->getContents()));
            return $res;
        }
    }

    /**
     *
     *
     * @param
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getEstEdades()
    {

        try {

            $client = new Client([
                'base_uri' => env('API_SERVER')
            ]);

            $res = $client->request('GET', '/api/cuadromando/grupo', [
                'auth' => ['mando_nora', 'mando_4p1n0r4'],
                'headers' => ['Content-Type' => 'application/json']
            ]);

            if ($res && $res->getStatusCode() == '200') {
                $res = ($res->getBody()->getContents());
                return $res;
            } else {
                return "nada";
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = (json_decode($e->getResponse()->getBody()->getContents()));
            return $res;
        }
    }
    
    /**
     *
     *
     * @param
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getEstDemografica()
    {

        try {

            $client = new Client([
                'base_uri' => env('API_SERVER')
            ]);

            $res = $client->request('GET', '/api/cuadromando/demo', [
                'auth' => ['mando_nora', 'mando_4p1n0r4'],
                'headers' => ['Content-Type' => 'application/json']
            ]);

            if ($res && $res->getStatusCode() == '200') {
                $res = ($res->getBody()->getContents());
                return $res;
            } else {
                return "nada";
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = (json_decode($e->getResponse()->getBody()->getContents()));
            return $res;
        }
    }

    public static function indicadores()
    {
        try {

            $client = new Client([
                'base_uri' => env('API_SERVER')
            ]);

            $res = $client->request('GET', '/api/cuadromando/indicadores', [
                'auth' => ['mando_nora', 'mando_4p1n0r4'],
                'headers' => ['Content-Type' => 'application/json']
            ]);

            if ($res && $res->getStatusCode() == '200') {
                $res = ($res->getBody()->getContents());
                return $res;
            } else {
                return "nada";
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = (json_decode($e->getResponse()->getBody()->getContents()));
            return $res;
        }
    }

    public static function CuestionariosExport($cuestionario)
    {
        try {
            
            $client = new Client([
                'base_uri' => env('API_SERVER')
            ]);
            $body = [
                'cuestionario'       => $cuestionario,
            ];

            $res = $client->request('GET', '/api/cuadromando/cuestionario', [
                'auth' => ['mando_nora', 'mando_4p1n0r4'],
                'headers' => ['Content-Type' => 'application/json'],
                'body' => json_encode($body)
            ]);

            if ($res && $res->getStatusCode() == '200') {
                $res = ($res->getBody()->getContents());
                return $res;
            } else {
                return "nada";
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = (json_decode($e->getResponse()->getBody()->getContents()));
            return $res;
        }
    }
    
}