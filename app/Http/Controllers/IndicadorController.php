<?php
namespace App\Http\Controllers;

use App\Exports\CuestionariosExport;
use App\Extensions\Curls;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Maatwebsite\Excel\Facades\Excel;

use function GuzzleHttp\json_encode;

class IndicadorController extends Controller
{

    public function downloadExcel()
    { 
        $data = Curls::getDataGlobal();
 
        return response($data,200)->header('Content-type','application/json');
    }

    public function genero()
    { 
        $data = Curls::getEstGenero();
 
        return response($data,200)->header('Content-type','application/json');
    }

    public function edades()
    { 
        $data = Curls::getEstEdades();
 
        return response($data,200)->header('Content-type','application/json');
    }

    public function demografica()
    { 
        $data = Curls::getEstDemografica();
        
        return response($data,200)->header('Content-type','application/json');
    }

    public function indicadores()
    { 
        $data = Curls::indicadores();
        
        return response($data,200)->header('Content-type','application/json');
    }

    public function pruebaOtro(Request $request)
    {
        return "aja";
    }
}