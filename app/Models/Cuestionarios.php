<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuestionarios extends Model
{
    use HasFactory;
    protected $connection = 'encuesta';
    protected $table = 'c0-bot';
}
