/**
 * Para su uso, visitar la documentacion de amcharts
 * Location-Sensitive Map https://www.amcharts.com/demos/location-sensitive-map/
 */
var root_demografica = am5.Root.new("distribucion-demografica");

root_demografica.setThemes([
    am5themes_Animated.new(root_demografica)
]);

var chart_demografica = root_demografica.container.children.push(am5map.MapChart.new(root_demografica, {
    panX: "rotateX",
    projection: am5map.geoMercator(),
    layout: root_demografica.horizontalLayout
}));

/* 
am5.net.load("https://cors-anywhere.herokuapp.com/https://www.amcharts.com/tools/country/?v=xz6Z", chart_demografica).then(function (result) {
    var geo = am5.JSONParser.parse(result.response);
    console.log(geo)
    loadGeodata(geo.country_code);
}); */
//cargamos el mapa de Panama especificamente
loadGeodata('PA');

var polygonSeries = chart_demografica.series.push(am5map.MapPolygonSeries.new(root_demografica, {
    calculateAggregates: true,
    valueField: "value"
}));

polygonSeries.mapPolygons.template.setAll({
    tooltipText: "{name}",
    interactive: true
});

polygonSeries.mapPolygons.template.states.create("hover", {
    fill: am5.color(0x677935)
});



polygonSeries.set("heatRules", [{
    target: polygonSeries.mapPolygons.template,
    dataField: "value",
    min: am5.color(0x8ab7ff),
    max: am5.color(0x25529a),
    key: "fill"
}]);

polygonSeries.mapPolygons.template.events.on("pointerover", function(ev) {
  heatLegend.showValue(ev.target.dataItem.get("value"));
});

polygonSeries.mapPolygons.template.setAll({
    tooltipText: "{name}: {value}%"
});
function loadGeodata(country) {
    
    // Default map
    var defaultMap = "panamaLow";
    
    if (country == "US") {
      chart_demografica.set("projection", am5map.geoAlbersUsa());
    }
    else {
      chart_demografica.set("projection", am5map.geoMercator());
    }
  
    // calculate which map to be used
    var currentMap = defaultMap;
    var title = "";
    if (am5geodata_data_countries2[country] !== undefined) {
      currentMap = am5geodata_data_countries2[country]["maps"][0];
  
      // add country title
      if (am5geodata_data_countries2[country]["country"]) {
        title = am5geodata_data_countries2[country]["country"];
      }
    }
    

      
      $.ajax({
        url:'demo',
        method:'GET'
        }).done(function(res){
          am5.net.load("https://cdn.amcharts.com/lib/5/geodata/json/" + currentMap + ".json", chart_demografica).then(function (result) {
          var geodata = am5.JSONParser.parse(result.response);
          var data = [];
          for(var i = 0; i < geodata.features.length; i++) {
            var prueba = geodata.features[i].id;
            prueba = res[i].id;
            data.push({
              id: prueba,
              value: parseFloat(res[i].value)
            });
          }

          polygonSeries.set("geoJSON", geodata);
          polygonSeries.data.setAll(data)
        });
    });
    
    chart_demografica.seriesContainer.children.push(am5.Label.new(root_demografica, {
      x: 5,
      y: 5,
      text: title,
      background: am5.RoundedRectangle.new(root_demografica, {
        fill: am5.color(0xffffff),
        fillOpacity: 0.2
      })
    }))
  
}

var heatLegend = chart_demografica.children.push(
    am5.HeatLegend.new(root_demografica, {
      orientation: "vertical",
      startColor: am5.color(0x8ab7ff),
      endColor: am5.color(0x25529a),
      startText: "Lowest",
      endText: "Highest",
      stepCount: 5
    })
);

heatLegend.startLabel.setAll({
    fontSize: 12,
    fill: heatLegend.get("startColor")
});
  
heatLegend.endLabel.setAll({
    fontSize: 12,
    fill: heatLegend.get("endColor")
});

polygonSeries.events.on("datavalidated", function () {
    heatLegend.set("startValue", polygonSeries.getPrivate("valueLow"));
    heatLegend.set("endValue", polygonSeries.getPrivate("valueHigh"));
});
