/**
 * Para su uso, visitar la documentacion de amcharts
 * Pie chart with Legend https://www.amcharts.com/demos/pie-chart-with-legend/
 */


 var root_grupos_edad = am5.Root.new("distribucion-grupos-edad");

 root_grupos_edad.setThemes([
    am5themes_Animated.new(root_grupos_edad)
  ]);

  var chart_grupos_edad = root_grupos_edad.container.children.push(
    am5percent.PieChart.new(root_grupos_edad,{
        layout: root_grupos_edad.verticalLayout,
    })
);


  var series_grupos_edad = chart_grupos_edad.series.push(am5percent.PieSeries.new(root_grupos_edad, {
    valueField: "value",
    categoryField: "name",
    legendValueText: "[bold]{valuePercentTotal.formatNumber('0')}%[/]"
  }));


/*-------------------------------------------------------------------------------/
********  Funcion con las configuraciones principales que aplicaremos ******** */
function init_config_grupos_edad(){
    /* Modificamos el formato de los labels */
    series_grupos_edad.labels.template.set(
        "text", 
        "[bold]{valuePercentTotal.formatNumber('0')}%[/]"
    );

    /* Modificamos el formato de los tooltips */
    series_grupos_edad.slices.template.set(
        "tooltipText", 
        "{name}: [bold]{valuePercentTotal.formatNumber('0')}%[/]"
    );
        
    series_grupos_edad.ticks.template.setAll({
        location: 1
    }); 

    /* Configuraciones de la leyenda */
    var legend_grupos_edad = chart_grupos_edad.children.push(am5.Legend.new(root_grupos_edad, {
        centerX: am5.percent(50),
        x: am5.percent(50),
        marginTop: 15,
        marginBottom: 15,
    }));
    legend_grupos_edad.data.setAll(series_grupos_edad.dataItems);
}

  
fetch('edad')
  .then((resultado) => {
        return resultado.json();
    })
    .then((data) => {
        data = data.map(function(obj) {
            obj['name'] = obj['label']; 
            delete obj['label']; 
            return obj;
        });
        series_grupos_edad.data.setAll(data);
        
        init_config_grupos_edad();
    })
    .catch(function(error) {
        console.log(error);
    });


      
    
    series_grupos_edad.appear(1000, 100);
