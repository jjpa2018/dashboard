/**
 * Para su uso, visitar la documentacion de amcharts
 * Pie chart with Legend https://www.amcharts.com/demos/pie-chart-with-legend/
 */


 var root_sexo = am5.Root.new("distribucion-sexo");

  root_sexo.setThemes([
    am5themes_Animated.new(root_sexo)
  ]);

  var chart_sexo = root_sexo.container.children.push(
    am5percent.PieChart.new(root_sexo,{
        layout: root_sexo.verticalLayout,
    })
  );

  var series_sexo = chart_sexo.series.push(am5percent.PieSeries.new(root_sexo, {
    valueField: "value",
    categoryField: "name",
    legendValueText: "[bold]{valuePercentTotal.formatNumber('0')}%[/]"
  }));

  
/*-------------------------------------------------------------------------------/
********  Funcion con las configuraciones principales que aplicaremos ******** */
function init_config_sexo(){
    /* Modificamos el formato de los labels */
    series_sexo.labels.template.set(
        "text", 
        "[bold]{valuePercentTotal.formatNumber('0')}%[/]"
    );

    /* Modificamos el formato de los tooltips */
    series_sexo.slices.template.set(
        "tooltipText", 
        "{name}: [bold]{valuePercentTotal.formatNumber('0')}%[/]"
    );
        
    series_sexo.ticks.template.setAll({
        location: 1
    }); 

    /* Configuraciones de la leyenda */
    var legend_sexo = chart_sexo.children.push(am5.Legend.new(root_sexo, {
        centerX: am5.percent(50),
        x: am5.percent(50),
        marginTop: 15,
        marginBottom: 15,
    }));
    legend_sexo.data.setAll(series_sexo.dataItems);
}


fetch('genero')
  .then((resultado) => {
        return resultado.json();
    })
    .then((data) => {
      data = data.map(function(obj) {
        obj['name'] = obj['label']; 
        delete obj['label']; 
        return obj;
      });
      series_sexo.data.setAll(data);
      init_config_sexo();
    })
    .catch(function(error) {
        console.log(error);
    });


      
    
    series_sexo.appear(1000, 100);
