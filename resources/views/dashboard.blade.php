<x-app-layout title="Dashboard">
<style>
    #chartdiv {
        width: 100%;
        max-height: 600px;
        height: 100vh;
    }
    #distribucion-grupos-edad {
        width: 100%;
        height: 320px;
    }
    #distribucion-sexo {
        width: 100%;
        height: 320px;
    }
    #distribucion-demografica {
        width: 100%;
        height: 500px
    }


</style>

    <div class="container grid px-6 mx-auto">
        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">        </h2>

        <!-- Cards -->
        <div class="grid gap-6 mb-6 md:grid-cols-3 xl:grid-cols-3">
            <!-- Card -->
            <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                <div class="p-3 mr-4 text-orange-500 bg-orange-100 rounded-full dark:text-orange-100 dark:bg-orange-500">
                    <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                        <path d="M16,7V3H14V7H10V3H8V7H8C7,7 6,8 6,9V14.5L9.5,18V21H14.5V18L18,14.5V9C18,8 17,7 16,7Z">
                        </path>
                    </svg>
                </div>
                <div>
                    <p class="text-sm font-medium text-gray-600 dark:text-gray-400">
                        Universo
                    </p>
                    <p class="text-lg font-semibold text-gray-700 dark:text-gray-200" id="universo">
                        
                    </p>
                </div>
            </div>
            <!-- Card -->
            <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-full dark:text-green-100 dark:bg-green-500">
                <svg class="w-5 h-5" viewBox="0 0 24 24">
                    <path fill="currentColor" d="M12,3C17.5,3 22,6.58 22,11C22,15.42 17.5,19 12,19C10.76,19 9.57,18.82 8.47,18.5C5.55,21 2,21 2,21C4.33,18.67 4.7,17.1 4.75,16.5C3.05,15.07 2,13.13 2,11C2,6.58 6.5,3 12,3M17,12V10H15V12H17M13,12V10H11V12H13M9,12V10H7V12H9Z" />
                </svg>
                </div>
                <div>
                    <p class="text-sm font-medium text-gray-600 dark:text-gray-400">
                        Participantes Activos
                    </p>
                    <p class="text-lg font-semibold text-gray-700 dark:text-gray-200" id="participantes-activos">
                        
                    </p>
                </div>
            </div>
            <!-- Card -->
            <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                <div class="p-3 mr-4 text-blue-500 bg-blue-100 rounded-full dark:text-blue-100 dark:bg-blue-500">
                    <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                        <path d="M17,12V3A1,1 0 0,0 16,2H3A1,1 0 0,0 2,3V17L6,13H16A1,1 0 0,0 17,12M21,6H19V15H6V17A1,1 0 0,0 7,18H18L22,22V7A1,1 0 0,0 21,6Z">
                        </path>
                    </svg>
                </div>
                <div>
                    <p class="text-sm font-medium text-gray-600 dark:text-gray-400">
                        Participantes Excluidos
                    </p>
                    <p class="text-lg font-semibold text-gray-700 dark:text-gray-200" id="participantes-excluidos">
                        
                    </p>
                </div>
            </div>
            <!-- Card -->
        </div>

        <!-- Graficos -->
        <div class="grid gap-12 mb-12 md:grid-cols-1">

        <!-- Distribucion demografica -->
        <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <h4 class="mb-4 font-semibold text-gray-800 dark:text-gray-300">
                Distribución Demografica
            </h4>
            <div id="distribucion-demografica"></div>
        </div>

        </div>
    <div class="grid gap-12 mb-12 md:grid-cols-2">

        <!-- Distribucion porcentual por grupos de edad -->
        <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <h4 class="mb-4 font-semibold text-gray-800 dark:text-gray-300">
                Distribucion Porcentual por Grupos de Edad
            </h4>
            <div id="distribucion-grupos-edad"></div>
        </div>

        <!-- Distribucion porcentual por sexo -->
        <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <h4 class="mb-4 font-semibold text-gray-800 dark:text-gray-300">
                Distribucion Porcentual por Sexo
            </h4>
            <div id="distribucion-sexo"></div>
        </div>
        
    </div>
<br>
<br>



    {{-- <div class="py-12">
        <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
            <div class="overflow-hidden bg-white shadow-xl sm:rounded-lg">
                <x-jet-welcome />
            </div>
        </div>
    </div> --}}
</x-app-layout>
