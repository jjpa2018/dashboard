<div class="py-4 text-gray-500 dark:text-gray-400">
    <!-- <a class="ml-6 text-lg font-bold text-gray-800 dark:text-gray-200" href="#">
        {{ config('app.name') }}
    </a> -->
<!--     <div class="p-2 rounded flex justify-center items-center flex-col">
        <a class="ml-6 text-lg font-bold text-gray-800 dark:text-gray-200" href="#">
        <img aria-hidden="true" style="width:100px; height:100px" class="text-gray-600 mb-2" src="{{ asset('img/logo-nora.png') }}" alt="Nora" />
        </a>
        <a class="ml-6 text-lg font-bold text-gray-800 dark:text-gray-200" href="#">
        {{ config('app.name') }}
    </a>                     
    </div> -->

    <div class="flex space-2 items-center pb-4">
      <div>
        <a class="ml-6 text-lg font-bold text-gray-800 dark:text-gray-200" href="#">
         <img aria-hidden="true" class="ml-2 h-14 w-14" class="text-gray-600 mb-2" src="{{ asset('img/logo-nora.png') }}" alt="Nora" />
        </a>
      </div>
      <div class="ml-2 mt-5">
        <h1 class="text-2xl font-bold">N.O.R.A</h1>
        <p class="text-center text-sm">DASHBOARD</p>
      </div>
    </div>

    <div class="border-t border-gray-100"></div>
    <ul class="mt-6">
        <li class="relative px-6 py-3">
            {!! request()->routeIs('dashboard') ? '<span class="absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
            <a data-turbolinks-action="replace" class="inline-flex items-center w-full text-sm font-semibold text-gray-800 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100" href="{{route('dashboard')}}">
                <i class="fa-solid fa-house-chimney"></i>
                <span class="ml-4">{{ __('Dashboard') }}</span>
            </a>
        </li>
        <li class="relative px-6 py-3">
            {!! request()->routeIs('indicadores') ? '<span class="absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
            <a data-turbolinks-action="replace" class="inline-flex items-center w-full text-sm font-semibold text-gray-800 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 dark:text-gray-100" href="{{route('indicadores')}}">
                <i class="fa-solid fa-rectangle-list"></i>
                <span class="ml-4">{{ __('Indicadores') }}</span>
            </a>
        </li>
    </ul>
</div>
