<?php

use App\Exports\CuestionarioExportC0;
use App\Exports\CuestionarioExportC1;
use App\Exports\CuestionarioExportC2;
use App\Exports\CuestionarioExportC3;
use App\Exports\CuestionarioExportC4A1;
use App\Exports\CuestionarioExportC4A2;
use App\Exports\CuestionarioExportC4B1;
use App\Exports\CuestionarioExportC4B2;
use App\Exports\CuestionarioExportC4B3;
use App\Exports\CuestionarioExportCR1A;
use App\Exports\CuestionarioExportCR1B;
use App\Exports\CuestionarioExportCR2A;
use App\Exports\CuestionarioExportCR2B;
use App\Exports\CuestionarioExportC5;
use App\Extensions\Curls;
use App\Http\Controllers\IndicadorController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});


Route::get('/pruebaStorage',[IndicadorController::class,'pruebaOtro']);


Route::get('/data',[IndicadorController::class,'downloadExcel'])->name('principal');
Route::get('/indicador',[IndicadorController::class,'indicadores'])->name('indicador');
Route::get('/genero',[IndicadorController::class,'genero'])->name('sex');
Route::get('/edad',[IndicadorController::class,'edades'])->name('edad');
Route::get('/demo',[IndicadorController::class,'demografica'])->name('demo');
Route::get('/cuestionario/{clave}',function($cuestionario){
    $now = Carbon::now();
    $fecha = $now->format('d-m-Y');
    $data = json_decode(Curls::CuestionariosExport($cuestionario),true);
    if (!empty($data)) {
        if($cuestionario == 'C0'){
            $export = new CuestionarioExportC0($data);
        }
        if($cuestionario == 'C1'){
            $export = new CuestionarioExportC1($data);
        }
        if($cuestionario == 'C2'){
            $export = new CuestionarioExportC2($data);
        }
        if($cuestionario == 'C3'){
            $export = new CuestionarioExportC3($data);
        }
        if($cuestionario == 'C4A1'){
            $export = new CuestionarioExportC4A1($data);
        }
        if($cuestionario == 'C4A2'){
            $export = new CuestionarioExportC4A2($data);
        }
        if($cuestionario == 'CR1A'){
            $export = new CuestionarioExportCR1A($data);
        }
        if($cuestionario == 'CR1B'){
            $export = new CuestionarioExportCR1B($data);
        }
        if($cuestionario == 'CR2A'){
            $export = new CuestionarioExportCR2A($data);
        }
        if($cuestionario == 'CR2B'){
            $export = new CuestionarioExportCR2B($data);
        }
        if($cuestionario == 'C4B1'){
            $export = new CuestionarioExportC4B1($data);
        }
        if($cuestionario == 'C4B2'){
            $export = new CuestionarioExportC4B2($data);
        }
        if($cuestionario == 'C4B3'){
            $export = new CuestionarioExportC4B3($data);
        }
        if($cuestionario == 'C5'){
            $export = new CuestionarioExportC5($data);
        }       
        return Excel::download($export, $fecha.'_Cuestionario'.$cuestionario.'.xlsx');
    }else{
        return redirect('indicadores');
    }

})->name('question');

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');


Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    Route::view('dashboard', 'dashboard')->name('dashboard');
    Route::view('indicadores', '/indicadores/index')->name('indicadores');
    //Route::view('charts', 'charts')->name('charts');
});















